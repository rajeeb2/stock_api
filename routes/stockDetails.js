var express = require("express");
var router = express.Router();
var custom_constants = require("../constant");

router.post("/getStockList", function (req, res) {
	global.dbConfig.getConnection(async function (err, connection) {
		if (err) throw err;
		try {
			connection.query("call PRO_GetStockList()", function (error, results) {
				if (!error) {
					res.status(custom_constants.STATUS_CODE.OK).json({ status: true, data: results[0] });
				} else {
					res.status(custom_constants.STATUS_CODE.BAD_REQUEST).json(error);
				}
				connection.release();
			});
		} catch (error) {
			res.status(custom_constants.STATUS_CODE.BAD_REQUEST).json(error);
			throw error;
		}
	});
});




module.exports = router;
