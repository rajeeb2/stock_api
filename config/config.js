const config = require('../config.json');
const environment = process.env.NODE_ENV || 'local';
const environmentConfig = config[environment];
module.exports = environmentConfig;